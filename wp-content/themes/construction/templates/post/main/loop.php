<div class="post-row">
<!-- 	<?php include(locate_template( 'templates/post/main/loop-date.php' )); ?> -->

	<div class="post-row-center text-center">
			<?php $images = get_field('portfolio_gallery', $post_data['p']->ID);
				$count = count($images); ?>
				
				<?php if( $images ): ?>
				<div class="portfolio-media">
					<div class="portfolio_main">
						<?php foreach( $images as $image ): ?>
							<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php endforeach; ?>
					</div>
					<?php if( $count > 1): ?>
					
				    <ul class="portfolio_gallery">
				        <?php foreach( $images as $image ): ?>
				            <li>
<!-- 				                <a href="<?php echo $image['url']; ?>"> -->
				                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
<!-- 				                </a> -->
				            </li>
				        <?php endforeach; ?>
				    </ul>
				    
				</div>
				<?php endif; ?>
				<?php endif; ?>
<!--
				<div class='media-inner'>
					<?php if ( has_post_format( 'image' ) ): ?>
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute()?>">
					<?php endif ?>

					<?php echo $post_data['media']; // xss ok ?>

					<?php if ( has_post_format( 'image' ) ): ?>
						</a>
					<?php endif ?>
				</div>
-->
		<div class="post-content-outer">
			<?php
// 				include(locate_template( 'templates/post/header-large.php' ));
				include(locate_template( 'templates/post/content.php' ));
				include(locate_template( 'templates/post/meta-loop.php' ));
			?>
		</div>
	</div>
</div>