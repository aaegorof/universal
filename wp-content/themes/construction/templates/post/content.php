<?php
/**
 * Post content template
 *
 * @package wpv
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$page_links = WpvTemplates::custom_link_pages( array(
	'before' => '<div class="wp-pagenavi"><span class="visuallyhidden">' . __( 'Pages:', 'construction' ) . '</span>',
	'after' => '</div>',
	'echo' => false,
) );

// if ( empty( $post_data['content'] ) && isset( $post_data['media'] ) && ( is_single() ? !WpvTemplates::has_share( 'post' ) : true ) && empty( $page_links ) ) return;

?>

<div class="post-content the-content">
	<?php
		do_action( 'wpv_before_post_content' );?>
		

		<div class="portfolio_content">
			<?php 
			if( have_rows('portfolio_addresses') ):

		 	// loop through the rows of data
		    while ( have_rows('portfolio_addresses')) : the_row();
			?>
			<div class="address">
				<?php the_sub_field('portfolio_address');?>
			</div>
			<?php endwhile; 
				else :
			    // no rows found
				endif;
			?>
			
			
			<?php if(get_field('portfolio_project')){
				echo '<div class="project">';
				the_field('portfolio_project');
				echo '</div>';
				}; 
			?>
			<?php if(get_field('portfolio_customer')){
				echo '<div class="customer">';
				the_field('portfolio_customer');
				echo '</div>';
				}; 
			?>
			<?php if(get_field('portfolio_website')){
				echo '<a class="portfolio-link" href="'. get_field('portfolio_website') . '" target="_blank" rel="nofollow">';
				the_field('portfolio_website');
				echo '</a>';
				}; 
			?>
			
		</div>
		<?php
		do_action( 'wpv_after_post_content' );

		echo $page_links; // xss ok
	?>
</div>