<?php 
$args = array(
/*
    'meta_key'          => 'sort_num',
    'orderby'           => 'meta_value_num', 
*/
	'orderby'           => 'description', 
    'hide_empty'        => false,
);
	
$terms = get_terms('portfolio_category', $args);
 echo '<div class="row">';
$i = 0;
 foreach ($terms as $term) {

 	$id = $term->term_id;
 	$image = get_field('portfolio-cat-img' , 'portfolio_category_'.$id);
 	$imageUrl = $image["url"];

 	echo '<div class="grid-1-3 portfolio-wrap" style="background-image: url('. $imageUrl .')"><a href="' . get_term_link( $term ) . '"><div class="portfolio-title">' . $term->name. '</div></a></div>'; 	


 };
 echo '</div>';

?>