<?php 
global $post;

$wpv_title = single_term_title('', 0);

get_header();

$termId = get_queried_object()->term_id;

$posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'portfolio',
    'meta_key' => 'portfolio_id', 
/*
	'meta_query' => array(
		'_portfolio_gallery' => array(
			'key'     => '_portfolio_gallery',
			'compare' => 'EXISTS',
		),
	),
*/
	'orderby' => 'meta_value',
    'order'  => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'term_id',
            'terms' => $termId,
        )
    )
)); 
?>

<div class="row page-wrapper porfolio-loop">
<?php foreach( $posts as $post ){ setup_postdata($post);?>
	<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() ); ?>>

	<div class="page-content">
		<?php $images = get_field('portfolio_gallery', $post->ID);
			$count = count($images); ?>
			
			<?php if( $images ): ?>
			<div class="portfolio-media">
				<div class="portfolio_main" id="p-main-<?php the_ID();?>">
					<?php foreach( $images as $image ): ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endforeach; ?>
				</div>
				<?php if( $count > 1): ?>
				
			    <ul class="portfolio_gallery" id="p-gallery-<?php the_ID();?>">
			        <?php foreach( $images as $image ): ?>
			            <li>
		                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			            </li>
			        <?php endforeach; ?>
			    </ul>
			    <?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="portfolio_content text-center">
			<?php 
			if( have_rows('portfolio_addresses') ):
			
				// loop through the rows of data
			while ( have_rows('portfolio_addresses')) : the_row();
			?>
			<div class="address">
				<?php the_sub_field('portfolio_address');?>
			</div>
			<?php endwhile; 
				else :
			    // no rows found
				endif;
			?>
			
			
			<?php if(get_field('portfolio_project')){
				echo '<div class="project">';
				the_field('portfolio_project');
				echo '</div>';
				}; 
			?>
			<?php if(get_field('portfolio_customer')){
				echo '<div class="customer">';
				the_field('portfolio_customer');
				echo '</div>';
				}; 
			?>
			<?php if(get_field('portfolio_website')){
				echo '<a class="portfolio-link" href="'. get_field('portfolio_website') . '" target="_blank" rel="nofollow">';
				the_field('portfolio_website');
				echo '</a>';
				}; 
			?>
			
		</div>
<!--
	<div>
		<?php the_field('portfolio_gallery'); ?>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</div>
-->
	
	</article>
<?php }
wp_reset_postdata();
?>

</div>


	



<?php get_footer(); ?>