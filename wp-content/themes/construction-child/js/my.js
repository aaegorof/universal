(function($) {
	$(document).ready(function(){
		$('.portfolio-media').each(function(){
			var mId = $(this).find('.portfolio_main').attr('id');
			var gId = $(this).find('.portfolio_gallery').attr('id');
			
			$(this).find('.portfolio_main').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
			  fade: true,
			  asNavFor: '#'+gId
			});
			$(this).find('.portfolio_gallery').slick({
			  slidesToShow: 5,
			  slidesToScroll: 1,
			  asNavFor: '#'+mId,
			  dots: true,
			  focusOnSelect: true
			});
			
		});
	});
})(jQuery);
