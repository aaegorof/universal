<?php

function child_styles() {
	wp_enqueue_style( 'my-child-theme-style', get_stylesheet_directory_uri() . '/style.css', array('front-all'), false, 'all' );
	wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/slick-theme.css', array(), false, 'all' );
	wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array(), '1.6.0', true);
	wp_enqueue_script( 'my', get_stylesheet_directory_uri(). '/js/my.js', array(), '', true);

}
add_action('wp_enqueue_scripts', 'child_styles', 11);
